<?php


    class Person {

        public $myProperty="A1", $myP1="A2", $myP2="A3", $myP3="A4";

        public function __sleep()
        {
            echo "I'm inside ".__METHOD__. "<br>";
            return array('myP1','myP3');  //return is a must here

        }

        public function __wakeup()
        {
            echo "Database connected again"."<br>";
        }

        public static function __callStatic($name, $arguments)
        {
            echo "I'm inside ".__METHOD__. "<br>";
            echo $name . "  ". "<br>"."<br>";

            echo "<pre>";
            var_dump($arguments);
            echo "</pre>";
        }

        public static  function doiSomethingStatic(){

            echo "I'm inside ".__METHOD__. "<br>";
        }

        public function __call($name, $arguments)
        {
            echo "I'm inside ".__METHOD__. "<br>";
            echo $name . "  ". "<br>"."<br>";

            echo "<pre>";
            var_dump($arguments);
            echo "</pre>";
        }

        public function __isset($name)
        {
            echo "I'm inside ".__METHOD__. "<br>";
            echo $name . "  ". "<br>";
        }

        public  function __set($name, $value)
        {
            // TODO: Implement __set() method.
            echo "I'm inside ".__METHOD__. "<br>";
            echo $name . $value . "<br>";
        }

        public  function __get($name)
        {
            // TODO: Implement __get() method.
            echo "I'm inside ".__METHOD__. "<br>"."<br>";
            echo $name . "  ". "<br>"."<br>";
        }

        public  function  __construct()
        {
            echo "Datbase connection succesful" ."<br>"."<br>";
        }

        public function __destruct()
        {
            echo "Maaf kore diyen.". __METHOD__."<br>"."<br>";
        }

        public function doSomething(){

            echo "i'm doing something from ". __METHOD__."<br>"."<br>";
        }
    }

    $objPerson1 = new Person();
    $objPerson2 = new Person();
    $objPerson3 = new Person();

    unset($objPerson2);

    $objPerson1->doSomething();
    echo $objPerson1->aPropertyThatDoesNotExist = "Hello World" ;

    echo "<br>";
    if( isset($objPerson1->aPropertyThatDoesNotExist)){

    } else{

    }

    unset($objPerson1->aPropertyThatDoesNotExist);

    $objPerson1->doingSomething("Hello world",123,true,12.3);

    Person::doSomethingStatic();
    Person::doSomethingDoesNotExist("gate",34);

    serialize($objPerson1);

    $myVar = serialize($objPerson1);
    var_dump($myVar);
    echo "<br>";


    $myPerson1Object = unserialize($myVar);
    var_dump($myPerson1Object);

    echo "<hr>";
//set magic method